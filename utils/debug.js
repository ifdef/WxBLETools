// 打印模块
const DEBUG_DEBUG = 0;  // debug打印等级
const DEBUG_INFO = 1; // info打印等级
const DEBUG_WARNING = 2; // warning打印等级
const DEBUG_ERROR = 3; // 错误打印等级

const debug_level = DEBUG_ERROR; // 控制的打印级别，比这个小的打印等级都不能打印输出

/**
 * 打印输出，包含模块，打印等级和打印内容
 * @param {*} moduleName 
 * @param {*} debuglevel 
 * @param  {...any} value 
 */
function Debug(moduleName, debuglevel,...value) {
    if (debuglevel >= debug_level) {
        console.log('['+moduleName+'] '+value);
    }
}

module.exports = {
    DEBUG_DEBUG,
    DEBUG_INFO,
    DEBUG_WARNING,
    DEBUG_ERROR,
    Debug
}