// index.js
// 获取应用实例
const app = getApp();
var permisson = require('permission.js');
var ble = require('../../utils/ble.js');
var debug = require('../../utils/debug.js');

const INDEX_MODUAL_NAME = "index";
const blePermissionName = 'scope.bluetooth'; // 蓝牙对应的权限名称
const blePermissionZhName = '蓝牙'; // 蓝牙权限对应的中文名称

Page({
  // 和界面相关的变量

  /**
      devList属性：
      dev_id: deviceId,  // 设备ID
      dev_name: 'Dev 1', // 设备名称
      dev_mac: '00:00:00:00:00:00', // 设备MAC地址
      dev_rssi: '-00db', // 设备信号强度
      dev_br: '' // 设备广播数据
   */
  data: {
    devlist:[
      // {
      //   dev_id: '0000',  // 设备ID
      //   dev_name: 'Dev 1', // 设备名称
      //   dev_mac: '00:00:00:00:00:00', // 设备MAC地址
      //   dev_rssi: '-00db', // 设备信号强度
      //   dev_br: '' // 设备广播数据
      // }
    ],
    bleSearchButtonObj: {
      loadingFlag: false,
      text: '搜索'
    }
  },

  // 非界面相关的变量
  userdata: {

  },

  // 设备列表元素点击事件
  devClick:function(e) {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "按了：", e.currentTarget.id);
    // 停止搜索
    if (this.data.bleSearchButtonObj.loadingFlag) {
      this.data.bleSearchButtonObj.loadingFlag = false
      this.data.bleSearchButtonObj.text = '搜索'
      this.setData({"bleSearchButtonObj": this.data.bleSearchButtonObj})
      ble.StopDiscovery();
    }

    // “保留当前页面，跳转到应用内的某个页面”，并且带一个返回按键，可传递参数
    wx.navigateTo({
      url: '../blectr/blectr?devId='+e.currentTarget.id
    });

    // if (ble.getDevConStaus(e.currentTarget.id) == ble.BLE_CON_SUCCESS) {
    //   ble.disconnect(e.currentTarget.id);
    // } else {
    //   ble.connect(e.currentTarget.id, 10000, (deviceId, result) => {
    //     if (result == 0) { // 连接成功
    //       debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "连接成功",deviceId, result);
    //       ble.getDevServices(deviceId, 
    //         (res) => {
    //           if (res != null) {  // service获取成功
    //             debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "设备服务", deviceId, res.services);
    //             ble.getDevChars(deviceId, res.services[0].uuid, (charResult) => {
    //                 if (charResult != null) { // 特征值获取成功
    //                     debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "设备特征值", deviceId, charResult.characteristics);
    //                 }
    //             })
    //           }
    //         });
    //     }
    //   });
    // }
  },

  serchButtonClick:function(e) {
    if (this.data.bleSearchButtonObj.loadingFlag == false){
      debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "开始搜索");
      wx.showLoading({
        title: '开始搜索',
      })

      // 每次都去检查一下权限，防止用户中途关闭权限
      /**
       * 必须使用箭头函数，否则this拿不到data对象
       * 箭头函数没有原型 即没有绑定this
       */
      permisson.permission_check(blePermissionName,
        (res) => {
          if (res) {  
              // 有权限，就去初始化蓝牙
              ble.Init((res) => {
                wx.hideLoading();
                if (res) {
                  this.data.bleSearchButtonObj.loadingFlag = true
                  this.data.bleSearchButtonObj.text = '停止'
                  this.setData({['bleSearchButtonObj']: this.data.bleSearchButtonObj})
                  this.setData({devlist:[]})  // 清空搜索列表，重新搜索

                  ble.StartDiscovery((res) => {
                        // debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "devList", this.data.devlist);
                        res.devices.forEach((device) => {
                          // debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "搜索到", device);
                          if (typeof device.localName == 'undefined' || device.localName.length == 0) {
                              return;
                          }
                          // 在devList中查找是否有该设备
                          var index = 0;
                          for (index=0; index<this.data.devlist.length; index++) {
                              if (device.deviceId == this.data.devlist[index].dev_id) {
                                break;
                              }
                          }

                          // 获取广播数据
                          var brBuffer = new Uint8Array(device.advertisData);
                          var brData = "";
                          for (var i=0; i<brBuffer.length; i++) {
                            if (brBuffer[i] < 16) { // 如果该变量小于16,在前面补0
                              brData += '0';
                            }
                            brData += brBuffer[i].toString(16) + " "; // 转换为16进制的字符串
                          }

                          // 如果数组没有，就新增，否则就更新信号强度和广播数据，有些设备广播数据是动态改变的
                          if (this.data.devlist.length == 0 || index == this.data.devlist.length) {
                            debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "搜索到可用的新设备", device.localName);

                            // console.log(device);
                            var devMac = "";
                            if (device.deviceId.indexOf(':') != -1) { // 如果deviceId包含分号，表示为Android平台，直接就为MAC地址
                              devMac = device.deviceId;
                            } else {
                              // 如果为IOS，截取设备ID的最后6个字节当做MAC地址，小程序没有办法获取MAC地址
                              var deviceIdTmp = device.deviceId.split('-');
                              for (var i=0; i<12; i++) {
                                if (deviceIdTmp == "" || deviceIdTmp.length < 4) {
                                  devMac += '0';
                                } else {
                                  if (i >= deviceIdTmp[4].length){
                                    devMac += '0';
                                  } else {
                                    devMac += deviceIdTmp[4].charAt(i);
                                  }
                                }
                                
                                // 加入分割符
                                if ((i != 11) && (i % 2) != 0) {
                                  devMac += ':';
                                }
                              }
                            }

                            // debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "新增设备：" + device.localName);
                            this.data.devlist.push({
                              dev_id: device.deviceId,
                              dev_name: device.localName,
                              dev_mac: devMac,
                              dev_rssi: device.RSSI,
                              dev_br: brData
                            });
                          } else {
                            this.data.devlist[index].dev_rssi = device.RSSI;
                            this.data.devlist[index].dev_br = brData;
                          }

                          this.setData({devlist:this.data.devlist});
                        })
                      }
                    );
                  } else {
                    // 初始化蓝牙失败，弹出提示框，提示用户检查蓝牙是否打开，是否有对应的权限，IOS还需要检查微信是否有蓝牙权限
                    // wx.showToast({
                    //     title: '初始化蓝牙失败，请检查权限和蓝牙是否打开，请检查微信是否有蓝牙权限！',
                    //     icon: 'none',
                    //     duration: 2000
                    // });
                    wx.showModal({
                      title: "温馨提示", // 提示的标题
                      content: "初始化蓝牙失败，请检查权限和蓝牙是否打开。苹果用户请在手机“设置->隐私->蓝牙”中打开微信的蓝牙权限，否则不能正常使用！", // 提示的内容
                      showCancel: false, // 是否显示取消按钮，默认true
                      //cancelText: "取消", // 取消按钮的文字，最多4个字符
                      //cancelColor: "#000000", // 取消按钮的文字颜色，必须是16进制格式的颜色字符串
                      confirmText: "确定", // 确认按钮的文字，最多4个字符
                      confirmColor: "#576B95", // 确认按钮的文字颜色，必须是 16 进制格式的颜色字符串
                      success: function (res) {
                          if (res.confirm) {
                              console.log('用户点击确定')
                          }
                      }
                    });
                  }
                });
          } else {
            wx.hideLoading();
            // 如果没有该权限，就去请求该权限
            permisson.permission_request(blePermissionName, blePermissionZhName);
          }
        });
    } else {
      wx.hideLoading({});
      this.data.bleSearchButtonObj.loadingFlag = false
      this.data.bleSearchButtonObj.text = '搜索'
      this.setData({"bleSearchButtonObj": this.data.bleSearchButtonObj})
      ble.StopDiscovery();
    }
  },

  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "index onload");
    //请求相关权限
    permisson.permission_request(blePermissionName, blePermissionZhName);

    // wx.navigateTo({
    //   url: '../blectr/blectr'
    // });
  },

  onUnload() {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "index onUnload");
    ble.DeInit();
  },

  onShow() {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "index onShow");
  },

  onHide() {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "index onHide");
    //隐藏小程序的时候，蓝牙还会在后台搜索一会儿，然后才会停止，暂时不知道不处理会不会导致资源泄露或者影响功耗
    //隐藏的时候，就停止搜索
    if (this.data.bleSearchButtonObj.loadingFlag) {
      wx.hideLoading({});
      this.data.bleSearchButtonObj.loadingFlag = false
      this.data.bleSearchButtonObj.text = '搜索'
      this.setData({"bleSearchButtonObj": this.data.bleSearchButtonObj})
      ble.StopDiscovery();
    }
  },

  onSaveExitState() {
    debug.Debug(INDEX_MODUAL_NAME, debug.DEBUG_DEBUG, "idnex onSaveExitState");
  },

})