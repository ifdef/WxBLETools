// BLE常用UUID和对应的名称关系，UUID为128bit字符串
var debug = require('../../utils/debug.js');

const BLEUUID_MODUAL_NAME = "BLEUUID";
/*
var bleuuidMap = new HashMap();

// Sample Services.
bleuuidMap.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
bleuuidMap.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
// Sample Characteristics.
bleuuidMap.put("00002a37-0000-1000-8000-00805f9b34fb", "Heart Rate Measurement");
bleuuidMap.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");

// GATT Services
bleuuidMap.put("00001800-0000-1000-8000-00805f9b34fb", "Generic Access");
bleuuidMap.put("00001801-0000-1000-8000-00805f9b34fb", "Generic Attribute");


// GATT Declarations
bleuuidMap.put("00002800-0000-1000-8000-00805f9b34fb", "Primary Service");
bleuuidMap.put("00002801-0000-1000-8000-00805f9b34fb", "Secondary Service");
bleuuidMap.put("00002802-0000-1000-8000-00805f9b34fb", "Include");
bleuuidMap.put("00002803-0000-1000-8000-00805f9b34fb", "Characteristic");

// GATT Descriptors
bleuuidMap.put("00002900-0000-1000-8000-00805f9b34fb", "Characteristic Extended Properties");
bleuuidMap.put("00002901-0000-1000-8000-00805f9b34fb", "Characteristic User Description");
bleuuidMap.put("00002902-0000-1000-8000-00805f9b34fb", "Client Characteristic Configuration");
bleuuidMap.put("00002903-0000-1000-8000-00805f9b34fb", "Server Characteristic Configuration");
bleuuidMap.put("00002904-0000-1000-8000-00805f9b34fb", "Characteristic Presentation Format");
bleuuidMap.put("00002905-0000-1000-8000-00805f9b34fb", "Characteristic Aggregate Format");
bleuuidMap.put("00002906-0000-1000-8000-00805f9b34fb", "Valid Range");
bleuuidMap.put("00002907-0000-1000-8000-00805f9b34fb", "External Report Reference Descriptor");
bleuuidMap.put("00002908-0000-1000-8000-00805f9b34fb", "Report Reference Descriptor");

// GATT Characteristics
bleuuidMap.put("00002a00-0000-1000-8000-00805f9b34fb", "Device Name");
bleuuidMap.put("00002a01-0000-1000-8000-00805f9b34fb", "Appearance");
bleuuidMap.put("00002a02-0000-1000-8000-00805f9b34fb", "Peripheral Privacy Flag");
bleuuidMap.put("00002a03-0000-1000-8000-00805f9b34fb", "Reconnection Address");
bleuuidMap.put("00002a04-0000-1000-8000-00805f9b34fb", "PPCP");
bleuuidMap.put("00002a05-0000-1000-8000-00805f9b34fb", "Service Changed");

// GATT Service UUIDs
bleuuidMap.put("00001802-0000-1000-8000-00805f9b34fb", "Immediate Alert");
bleuuidMap.put("00001803-0000-1000-8000-00805f9b34fb", "Link Loss");
bleuuidMap.put("00001804-0000-1000-8000-00805f9b34fb", "Tx Power");
bleuuidMap.put("00001805-0000-1000-8000-00805f9b34fb", "Current Time Service");
bleuuidMap.put("00001806-0000-1000-8000-00805f9b34fb", "Reference Time Update Service");
bleuuidMap.put("00001807-0000-1000-8000-00805f9b34fb", "Next DST Change Service");
bleuuidMap.put("00001808-0000-1000-8000-00805f9b34fb", "Glucose");
bleuuidMap.put("00001809-0000-1000-8000-00805f9b34fb", "Health Thermometer");
bleuuidMap.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information");
bleuuidMap.put("0000180b-0000-1000-8000-00805f9b34fb", "Network Availability");
bleuuidMap.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate");
bleuuidMap.put("0000180e-0000-1000-8000-00805f9b34fb", "Phone Alert Status Service");
bleuuidMap.put("0000180f-0000-1000-8000-00805f9b34fb", "Battery Service");
bleuuidMap.put("00001810-0000-1000-8000-00805f9b34fb", "Blood Pressure");
bleuuidMap.put("00001811-0000-1000-8000-00805f9b34fb", "Alert Notification Service");
bleuuidMap.put("00001812-0000-1000-8000-00805f9b34fb", "Human Interface Device");
bleuuidMap.put("00001813-0000-1000-8000-00805f9b34fb", "Scan Parameters");
bleuuidMap.put("00001814-0000-1000-8000-00805f9b34fb", "Running Speed and Cadence");
bleuuidMap.put("00001816-0000-1000-8000-00805f9b34fb", "Cycling Speed and Cadence");
bleuuidMap.put("00001818-0000-1000-8000-00805f9b34fb", "Cycling Power");
bleuuidMap.put("00001819-0000-1000-8000-00805f9b34fb", "Location and Navigation");

// GATT Characteristic UUIDs
bleuuidMap.put("00002a06-0000-1000-8000-00805f9b34fb", "Alert Level");
bleuuidMap.put("00002a07-0000-1000-8000-00805f9b34fb", "Tx Power Level");
bleuuidMap.put("00002a08-0000-1000-8000-00805f9b34fb", "Date Time");
bleuuidMap.put("00002a09-0000-1000-8000-00805f9b34fb", "Day of Week");
bleuuidMap.put("00002a0a-0000-1000-8000-00805f9b34fb", "Day Date Time");
bleuuidMap.put("00002a0c-0000-1000-8000-00805f9b34fb", "Exact Time 256");
bleuuidMap.put("00002a0d-0000-1000-8000-00805f9b34fb", "DST Offset");
bleuuidMap.put("00002a0e-0000-1000-8000-00805f9b34fb", "Time Zone");
bleuuidMap.put("00002a0f-0000-1000-8000-00805f9b34fb", "Local Time Information");
bleuuidMap.put("00002a11-0000-1000-8000-00805f9b34fb", "Time with DST");
bleuuidMap.put("00002a12-0000-1000-8000-00805f9b34fb", "Time Accuracy");
bleuuidMap.put("00002a13-0000-1000-8000-00805f9b34fb", "Time Source");
bleuuidMap.put("00002a14-0000-1000-8000-00805f9b34fb", "Reference Time Information");
bleuuidMap.put("00002a16-0000-1000-8000-00805f9b34fb", "Time Update Control Point");
bleuuidMap.put("00002a17-0000-1000-8000-00805f9b34fb", "Time Update State");
bleuuidMap.put("00002a18-0000-1000-8000-00805f9b34fb", "Glucose Measurement");
bleuuidMap.put("00002a19-0000-1000-8000-00805f9b34fb", "Battery Level");
bleuuidMap.put("00002a1c-0000-1000-8000-00805f9b34fb", "Temperature Measurement");
bleuuidMap.put("00002a1d-0000-1000-8000-00805f9b34fb", "Temperature Type");
bleuuidMap.put("00002a1e-0000-1000-8000-00805f9b34fb", "Intermediate Temperature");
bleuuidMap.put("00002a21-0000-1000-8000-00805f9b34fb", "Measurement Interval");
bleuuidMap.put("00002a22-0000-1000-8000-00805f9b34fb", "Boot Keyboard Input Report");
bleuuidMap.put("00002a23-0000-1000-8000-00805f9b34fb", "System ID");
bleuuidMap.put("00002a24-0000-1000-8000-00805f9b34fb", "Model Number String");
bleuuidMap.put("00002a25-0000-1000-8000-00805f9b34fb", "Serial Number String");
bleuuidMap.put("00002a26-0000-1000-8000-00805f9b34fb", "Firmware Revision String");
bleuuidMap.put("00002a27-0000-1000-8000-00805f9b34fb", "Hardware Revision String");
bleuuidMap.put("00002a28-0000-1000-8000-00805f9b34fb", "Software Revision String");
bleuuidMap.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
bleuuidMap.put("00002a2a-0000-1000-8000-00805f9b34fb", "IEEE 11073-20601 Regulatory Certification Data List");
bleuuidMap.put("00002a2b-0000-1000-8000-00805f9b34fb", "Current Time");
bleuuidMap.put("00002a31-0000-1000-8000-00805f9b34fb", "Scan Refresh");
bleuuidMap.put("00002a32-0000-1000-8000-00805f9b34fb", "Boot Keyboard Output Report");
bleuuidMap.put("00002a33-0000-1000-8000-00805f9b34fb", "Boot Mouse Input Report");
bleuuidMap.put("00002a34-0000-1000-8000-00805f9b34fb", "Glucose Measurement Context");
bleuuidMap.put("00002a35-0000-1000-8000-00805f9b34fb", "Blood Pressure Measurement");
bleuuidMap.put("00002a36-0000-1000-8000-00805f9b34fb", "Intermediate Cuff Pressure");
bleuuidMap.put("00002a37-0000-1000-8000-00805f9b34fb", "Heart Rate Measurement");
bleuuidMap.put("00002a38-0000-1000-8000-00805f9b34fb", "Body Sensor Location");
bleuuidMap.put("00002a39-0000-1000-8000-00805f9b34fb", "Heart Rate Control Point");
bleuuidMap.put("00002a3e-0000-1000-8000-00805f9b34fb", "Network Availability");
bleuuidMap.put("00002a3f-0000-1000-8000-00805f9b34fb", "Alert Status");
bleuuidMap.put("00002a40-0000-1000-8000-00805f9b34fb", "Ringer Control Point");
bleuuidMap.put("00002a41-0000-1000-8000-00805f9b34fb", "Ringer Setting");
bleuuidMap.put("00002a42-0000-1000-8000-00805f9b34fb", "Alert Category ID Bit Mask");
bleuuidMap.put("00002a43-0000-1000-8000-00805f9b34fb", "Alert Category ID");
bleuuidMap.put("00002a44-0000-1000-8000-00805f9b34fb", "Alert Notification Control Point");
bleuuidMap.put("00002a45-0000-1000-8000-00805f9b34fb", "Unread Alert Status");
bleuuidMap.put("00002a46-0000-1000-8000-00805f9b34fb", "New Alert");
bleuuidMap.put("00002a47-0000-1000-8000-00805f9b34fb", "Supported New Alert Category");
bleuuidMap.put("00002a48-0000-1000-8000-00805f9b34fb", "Supported Unread Alert Category");
bleuuidMap.put("00002a49-0000-1000-8000-00805f9b34fb", "Blood Pressure Feature");
bleuuidMap.put("00002a4a-0000-1000-8000-00805f9b34fb", "HID Information");
bleuuidMap.put("00002a4b-0000-1000-8000-00805f9b34fb", "Report Map");
bleuuidMap.put("00002a4c-0000-1000-8000-00805f9b34fb", "HID Control Point");
bleuuidMap.put("00002a4d-0000-1000-8000-00805f9b34fb", "Report");
bleuuidMap.put("00002a4e-0000-1000-8000-00805f9b34fb", "Protocol Mode");
bleuuidMap.put("00002a4f-0000-1000-8000-00805f9b34fb", "Scan Interval Window");
bleuuidMap.put("00002a50-0000-1000-8000-00805f9b34fb", "PnP ID");
bleuuidMap.put("00002a51-0000-1000-8000-00805f9b34fb", "Glucose Feature");
bleuuidMap.put("00002a52-0000-1000-8000-00805f9b34fb", "Record Access Control Point");
bleuuidMap.put("00002a53-0000-1000-8000-00805f9b34fb", "RSC Measurement");
bleuuidMap.put("00002a54-0000-1000-8000-00805f9b34fb", "RSC Feature");
bleuuidMap.put("00002a55-0000-1000-8000-00805f9b34fb", "SC Control Point");
bleuuidMap.put("00002a5b-0000-1000-8000-00805f9b34fb", "CSC Measurement");
bleuuidMap.put("00002a5c-0000-1000-8000-00805f9b34fb", "CSC Feature");
bleuuidMap.put("00002a5d-0000-1000-8000-00805f9b34fb", "Sensor Location");
bleuuidMap.put("00002a63-0000-1000-8000-00805f9b34fb", "Cycling Power Measurement");
bleuuidMap.put("00002a64-0000-1000-8000-00805f9b34fb", "Cycling Power Vector");
bleuuidMap.put("00002a65-0000-1000-8000-00805f9b34fb", "Cycling Power Feature");
bleuuidMap.put("00002a66-0000-1000-8000-00805f9b34fb", "Cycling Power Control Point");
bleuuidMap.put("00002a67-0000-1000-8000-00805f9b34fb", "Location and Speed");
bleuuidMap.put("00002a68-0000-1000-8000-00805f9b34fb", "Navigation");
bleuuidMap.put("00002a69-0000-1000-8000-00805f9b34fb", "Position Quality");
bleuuidMap.put("00002a6a-0000-1000-8000-00805f9b34fb", "LN Feature");
bleuuidMap.put("00002a6b-0000-1000-8000-00805f9b34fb", "LN Control Point");
*/

const bleUuidBuffer = [
    // Sample Services.
    {uuid:"0000180d-0000-1000-8000-00805f9b34fb", name: "Heart Rate Service"},
    {uuid:"0000180a-0000-1000-8000-00805f9b34fb", name: "Device Information Service"},
    // Sample Characteristics.
    {uuid:"00002a37-0000-1000-8000-00805f9b34fb", name: "Heart Rate Measurement"},
    {uuid:"00002a29-0000-1000-8000-00805f9b34fb", name: "Manufacturer Name String"},

    // GATT Services
    {uuid:"00001800-0000-1000-8000-00805f9b34fb", name: "Generic Access"},
    {uuid:"00001801-0000-1000-8000-00805f9b34fb", name: "Generic Attribute"},


    // GATT Declarations
    {uuid:"00002800-0000-1000-8000-00805f9b34fb", name: "Primary Service"},
    {uuid:"00002801-0000-1000-8000-00805f9b34fb", name: "Secondary Service"},
    {uuid:"00002802-0000-1000-8000-00805f9b34fb", name: "Include"},
    {uuid:"00002803-0000-1000-8000-00805f9b34fb", name: "Characteristic"},

    // GATT Descriptors
    {uuid:"00002900-0000-1000-8000-00805f9b34fb", name: "Characteristic Extended Properties"},
    {uuid:"00002901-0000-1000-8000-00805f9b34fb", name: "Characteristic User Description"},
    {uuid:"00002902-0000-1000-8000-00805f9b34fb", name: "Client Characteristic Configuration"},
    {uuid:"00002903-0000-1000-8000-00805f9b34fb", name: "Server Characteristic Configuration"},
    {uuid:"00002904-0000-1000-8000-00805f9b34fb", name: "Characteristic Presentation Format"},
    {uuid:"00002905-0000-1000-8000-00805f9b34fb", name: "Characteristic Aggregate Format"},
    {uuid:"00002906-0000-1000-8000-00805f9b34fb", name: "Valid Range"},
    {uuid:"00002907-0000-1000-8000-00805f9b34fb", name: "External Report Reference Descriptor"},
    {uuid:"00002908-0000-1000-8000-00805f9b34fb", name: "Report Reference Descriptor"},

    // GATT Characteristics
    {uuid:"00002a00-0000-1000-8000-00805f9b34fb", name: "Device Name"},
    {uuid:"00002a01-0000-1000-8000-00805f9b34fb", name: "Appearance"},
    {uuid:"00002a02-0000-1000-8000-00805f9b34fb", name: "Peripheral Privacy Flag"},
    {uuid:"00002a03-0000-1000-8000-00805f9b34fb", name: "Reconnection Address"},
    {uuid:"00002a04-0000-1000-8000-00805f9b34fb", name: "PPCP"},
    {uuid:"00002a05-0000-1000-8000-00805f9b34fb", name: "Service Changed"},

    // GATT Service UUIDs
    {uuid:"00001802-0000-1000-8000-00805f9b34fb", name: "Immediate Alert"},
    {uuid:"00001803-0000-1000-8000-00805f9b34fb", name: "Link Loss"},
    {uuid:"00001804-0000-1000-8000-00805f9b34fb", name: "Tx Power"},
    {uuid:"00001805-0000-1000-8000-00805f9b34fb", name: "Current Time Service"},
    {uuid:"00001806-0000-1000-8000-00805f9b34fb", name: "Reference Time Update Service"},
    {uuid:"00001807-0000-1000-8000-00805f9b34fb", name: "Next DST Change Service"},
    {uuid:"00001808-0000-1000-8000-00805f9b34fb", name: "Glucose"},
    {uuid:"00001809-0000-1000-8000-00805f9b34fb", name: "Health Thermometer"},
    {uuid:"0000180a-0000-1000-8000-00805f9b34fb", name: "Device Information"},
    {uuid:"0000180b-0000-1000-8000-00805f9b34fb", name: "Network Availability"},
    {uuid:"0000180d-0000-1000-8000-00805f9b34fb", name: "Heart Rate"},
    {uuid:"0000180e-0000-1000-8000-00805f9b34fb", name: "Phone Alert Status Service"},
    {uuid:"0000180f-0000-1000-8000-00805f9b34fb", name: "Battery Service"},
    {uuid:"00001810-0000-1000-8000-00805f9b34fb", name: "Blood Pressure"},
    {uuid:"00001811-0000-1000-8000-00805f9b34fb", name: "Alert Notification Service"},
    {uuid:"00001812-0000-1000-8000-00805f9b34fb", name: "Human Interface Device"},
    {uuid:"00001813-0000-1000-8000-00805f9b34fb", name: "Scan Parameters"},
    {uuid:"00001814-0000-1000-8000-00805f9b34fb", name: "Running Speed and Cadence"},
    {uuid:"00001816-0000-1000-8000-00805f9b34fb", name: "Cycling Speed and Cadence"},
    {uuid:"00001818-0000-1000-8000-00805f9b34fb", name: "Cycling Power"},
    {uuid:"00001819-0000-1000-8000-00805f9b34fb", name: "Location and Navigation"},

    // GATT Characteristic UUIDs
    {uuid:"00002a06-0000-1000-8000-00805f9b34fb", name: "Alert Level"},
    {uuid:"00002a07-0000-1000-8000-00805f9b34fb", name: "Tx Power Level"},
    {uuid:"00002a08-0000-1000-8000-00805f9b34fb", name: "Date Time"},
    {uuid:"00002a09-0000-1000-8000-00805f9b34fb", name: "Day of Week"},
    {uuid:"00002a0a-0000-1000-8000-00805f9b34fb", name: "Day Date Time"},
    {uuid:"00002a0c-0000-1000-8000-00805f9b34fb", name: "Exact Time 256"},
    {uuid:"00002a0d-0000-1000-8000-00805f9b34fb", name: "DST Offset"},
    {uuid:"00002a0e-0000-1000-8000-00805f9b34fb", name: "Time Zone"},
    {uuid:"00002a0f-0000-1000-8000-00805f9b34fb", name: "Local Time Information"},
    {uuid:"00002a11-0000-1000-8000-00805f9b34fb", name: "Time with DST"},
    {uuid:"00002a12-0000-1000-8000-00805f9b34fb", name: "Time Accuracy"},
    {uuid:"00002a13-0000-1000-8000-00805f9b34fb", name: "Time Source"},
    {uuid:"00002a14-0000-1000-8000-00805f9b34fb", name: "Reference Time Information"},
    {uuid:"00002a16-0000-1000-8000-00805f9b34fb", name: "Time Update Control Point"},
    {uuid:"00002a17-0000-1000-8000-00805f9b34fb", name: "Time Update State"},
    {uuid:"00002a18-0000-1000-8000-00805f9b34fb", name: "Glucose Measurement"},
    {uuid:"00002a19-0000-1000-8000-00805f9b34fb", name: "Battery Level"},
    {uuid:"00002a1c-0000-1000-8000-00805f9b34fb", name: "Temperature Measurement"},
    {uuid:"00002a1d-0000-1000-8000-00805f9b34fb", name: "Temperature Type"},
    {uuid:"00002a1e-0000-1000-8000-00805f9b34fb", name: "Intermediate Temperature"},
    {uuid:"00002a21-0000-1000-8000-00805f9b34fb", name: "Measurement Interval"},
    {uuid:"00002a22-0000-1000-8000-00805f9b34fb", name: "Boot Keyboard Input Report"},
    {uuid:"00002a23-0000-1000-8000-00805f9b34fb", name: "System ID"},
    {uuid:"00002a24-0000-1000-8000-00805f9b34fb", name: "Model Number String"},
    {uuid:"00002a25-0000-1000-8000-00805f9b34fb", name: "Serial Number String"},
    {uuid:"00002a26-0000-1000-8000-00805f9b34fb", name: "Firmware Revision String"},
    {uuid:"00002a27-0000-1000-8000-00805f9b34fb", name: "Hardware Revision String"},
    {uuid:"00002a28-0000-1000-8000-00805f9b34fb", name: "Software Revision String"},
    {uuid:"00002a29-0000-1000-8000-00805f9b34fb", name: "Manufacturer Name String"},
    {uuid:"00002a2a-0000-1000-8000-00805f9b34fb", name: "IEEE 11073-20601 Regulatory Certification Data List"},
    {uuid:"00002a2b-0000-1000-8000-00805f9b34fb", name: "Current Time"},
    {uuid:"00002a31-0000-1000-8000-00805f9b34fb", name: "Scan Refresh"},
    {uuid:"00002a32-0000-1000-8000-00805f9b34fb", name: "Boot Keyboard Output Report"},
    {uuid:"00002a33-0000-1000-8000-00805f9b34fb", name: "Boot Mouse Input Report"},
    {uuid:"00002a34-0000-1000-8000-00805f9b34fb", name: "Glucose Measurement Context"},
    {uuid:"00002a35-0000-1000-8000-00805f9b34fb", name: "Blood Pressure Measurement"},
    {uuid:"00002a36-0000-1000-8000-00805f9b34fb", name: "Intermediate Cuff Pressure"},
    {uuid:"00002a37-0000-1000-8000-00805f9b34fb", name: "Heart Rate Measurement"},
    {uuid:"00002a38-0000-1000-8000-00805f9b34fb", name: "Body Sensor Location"},
    {uuid:"00002a39-0000-1000-8000-00805f9b34fb", name: "Heart Rate Control Point"},
    {uuid:"00002a3e-0000-1000-8000-00805f9b34fb", name: "Network Availability"},
    {uuid:"00002a3f-0000-1000-8000-00805f9b34fb", name: "Alert Status"},
    {uuid:"00002a40-0000-1000-8000-00805f9b34fb", name: "Ringer Control Point"},
    {uuid:"00002a41-0000-1000-8000-00805f9b34fb", name: "Ringer Setting"},
    {uuid:"00002a42-0000-1000-8000-00805f9b34fb", name: "Alert Category ID Bit Mask"},
    {uuid:"00002a43-0000-1000-8000-00805f9b34fb", name: "Alert Category ID"},
    {uuid:"00002a44-0000-1000-8000-00805f9b34fb", name: "Alert Notification Control Point"},
    {uuid:"00002a45-0000-1000-8000-00805f9b34fb", name: "Unread Alert Status"},
    {uuid:"00002a46-0000-1000-8000-00805f9b34fb", name: "New Alert"},
    {uuid:"00002a47-0000-1000-8000-00805f9b34fb", name: "Supported New Alert Category"},
    {uuid:"00002a48-0000-1000-8000-00805f9b34fb", name: "Supported Unread Alert Category"},
    {uuid:"00002a49-0000-1000-8000-00805f9b34fb", name: "Blood Pressure Feature"},
    {uuid:"00002a4a-0000-1000-8000-00805f9b34fb", name: "HID Information"},
    {uuid:"00002a4b-0000-1000-8000-00805f9b34fb", name: "Report Map"},
    {uuid:"00002a4c-0000-1000-8000-00805f9b34fb", name: "HID Control Point"},
    {uuid:"00002a4d-0000-1000-8000-00805f9b34fb", name: "Report"},
    {uuid:"00002a4e-0000-1000-8000-00805f9b34fb", name: "Protocol Mode"},
    {uuid:"00002a4f-0000-1000-8000-00805f9b34fb", name: "Scan Interval Window"},
    {uuid:"00002a50-0000-1000-8000-00805f9b34fb", name: "PnP ID"},
    {uuid:"00002a51-0000-1000-8000-00805f9b34fb", name: "Glucose Feature"},
    {uuid:"00002a52-0000-1000-8000-00805f9b34fb", name: "Record Access Control Point"},
    {uuid:"00002a53-0000-1000-8000-00805f9b34fb", name: "RSC Measurement"},
    {uuid:"00002a54-0000-1000-8000-00805f9b34fb", name: "RSC Feature"},
    {uuid:"00002a55-0000-1000-8000-00805f9b34fb", name: "SC Control Point"},
    {uuid:"00002a5b-0000-1000-8000-00805f9b34fb", name: "CSC Measurement"},
    {uuid:"00002a5c-0000-1000-8000-00805f9b34fb", name: "CSC Feature"},
    {uuid:"00002a5d-0000-1000-8000-00805f9b34fb", name: "Sensor Location"},
    {uuid:"00002a63-0000-1000-8000-00805f9b34fb", name: "Cycling Power Measurement"},
    {uuid:"00002a64-0000-1000-8000-00805f9b34fb", name: "Cycling Power Vector"},
    {uuid:"00002a65-0000-1000-8000-00805f9b34fb", name: "Cycling Power Feature"},
    {uuid:"00002a66-0000-1000-8000-00805f9b34fb", name: "Cycling Power Control Point"},
    {uuid:"00002a67-0000-1000-8000-00805f9b34fb", name: "Location and Speed"},
    {uuid:"00002a68-0000-1000-8000-00805f9b34fb", name: "Navigation"},
    {uuid:"00002a69-0000-1000-8000-00805f9b34fb", name: "Position Quality"},
    {uuid:"00002a6a-0000-1000-8000-00805f9b34fb", name: "LN Feature"},
    {uuid:"00002a6b-0000-1000-8000-00805f9b34fb", name: "LN Control Point"},
];

/**
 * 获取通用UUID对应的名称，如果没有对应的，将返回Unkown Name
 * @param {string} uuid 
 */
function getUuidName(uuid) {
    for (let i=0; i<bleUuidBuffer.length; i++) {
        if (bleUuidBuffer[i].uuid == uuid.toLocaleLowerCase()) {
            return bleUuidBuffer[i].name;
        }
    }

    return "Unkown Name";
}

module.exports = {
    getUuidName
}
